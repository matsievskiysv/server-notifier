Notify user about script result via email.
`stderr` goes to message body, `stdout` goes to attachment.

# Install

```bash
sudo apt install smartmontools sudo jq
su root -c "/sbin/adduser $USER sudo"
```

```bash
tmpfile=$(mktemp)
crontab -l > $tmpfile
echo "0 7 * * * $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/disk_usage.sh)" >> $tmpfile
echo "0 7 * * * $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/systemd_degraded.sh)" >> $tmpfile
echo "0 7 * * * $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/check_mail.sh)" >> $tmpfile
crontab - < $tmpfile
rm -f $tmpfile
```

```
tmpfile=$(mktemp)
sudo crontab -u root -l > $tmpfile
echo "0 7 * * * $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/check_mail.sh)" >> $tmpfile
echo "0 7 * * 1 $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/upgradable.sh)" >> $tmpfile
echo "0 1 * * * $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/smartmon_short.sh)" >> $tmpfile
echo "0 23 * * 6 $(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/smartmon_long.sh)" >> $tmpfile
sudo crontab -u root - < $tmpfile
rm -f $tmpfile
```

```bash
cat > /tmp/reboot-notifier.service << __EOF__
[Unit]
Description=Reboot notifier
After=network.target

[Service]
Type=oneshot
ExecStart=$(realpath notifier.py) $(realpath notifier.ini) $(realpath scripts/reboot.sh)

[Install]
WantedBy=multi-user.target
__EOF__
sudo mv /tmp/reboot-notifier.service /etc/systemd/system/reboot-notifier.service
sudo systemctl enable reboot-notifier.service
```
