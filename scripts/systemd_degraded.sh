#!/usr/bin/env bash

if [ $(systemctl is-system-running) == "degraded" ]
then
    >&2 echo 'Some systemd services failed'
    systemctl list-units --state=failed --no-pager --no-ask-password
    >&2 echo 'Use the following commands to examine the unit file:'
    >&2 echo 'systemctl status <unit.service>'
    >&2 echo 'journalctl --unit=<unit.service>'
    exit 1
fi
