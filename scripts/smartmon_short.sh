#!/usr/bin/env bash

PATH="$PATH:/sbin:/usr/sbin"

block_devices=$(lsblk --json | jq -r '.blockdevices|map(.name)[]')
for device in ${block_devices[@]}
do
    device_path=/dev/${device}
    if [[ $(smartctl -i --json=c $device_path | jq '.smart_support.available') != "false" ]]
    then
	smartctl --smart on --offlineauto on --saveauto on $device_path
	smartctl --test short --test force $device_path
    fi
done
