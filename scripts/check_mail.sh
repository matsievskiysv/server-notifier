#!/usr/bin/env bash

set -e

if [[ -f "/var/mail/$USER" ]]
then
    if [[ "$(messages -q 2>/dev/null)" != 0 ]]
    then
        >&2 echo 'There are unread system messages'
        messages 2>/dev/null
        >&2 echo 'To view the messages use the command:'
        >&2 echo 'mail'
        exit 1
    fi
fi
