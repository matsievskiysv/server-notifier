#!/usr/bin/env bash

journalctl -xe --no-pager 2>&1
echo '==========================================='
dmesg 2>&1
>&2 echo 'Server rebooted'
>&2 echo 'Log error messages are attached to this mail'

exit 1
