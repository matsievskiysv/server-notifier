#!/usr/bin/env bash

set -e

PATH="$PATH:/sbin:/usr/sbin"
rv=0

block_devices=$(lsblk --json | jq -r '.blockdevices|map(.name)[]')
for device in ${block_devices[@]}
do
    device_path=/dev/${device}
    if [[ $(smartctl -i --json=c $device_path | jq '.smart_support.available') != "false" ]]
    then
	if [[ $(smartctl -i --json=c $device_path | jq '.smart_support.enabled') == "false" ]]
	then
	    rv=1
	    >&2 echo "Smart disabled for disk $device_path"
	    smartctl --all $device_path
	fi
	if [[ $(smartctl -H --json=c $device_path | jq '.smart_status.passed') == "false" ]]
	then
	    rv=1
	    >&2 echo "Smart health problem with disk $device_path"
	    smartctl --all $device_path
	    >&2 echo "Disk replacement may be required"
	fi
    fi
done

exit $rv
