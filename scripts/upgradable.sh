#!/usr/bin/env bash

set -e

apt-get update 2>&1 > /dev/null
upgradable=$(apt list --upgradable 2>/dev/null | tail -n+2)
if [[ ! -z "$upgradable" ]]
then
    >&2 echo 'System packages may be upgraded'
    echo 'The following packages may be upgraded'
    echo "$upgradable"
    >&2 echo 'To upgrade the system use commands:'
    >&2 echo 'sudo apt update'
    >&2 echo 'sudo apt upgrade'
    exit 1
fi
