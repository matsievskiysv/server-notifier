#!/usr/bin/env bash

set -e

LIMIT=${DISK_USAGE_LIMIT:-90}

df -h | sed -e '2,${/^\/[^\/]/!d}'
exceed=$(df --output=source,pcent | sed -e '/^\/[^\/]/!d' -e 's/%//g' | awk '{if($2>'$LIMIT'){print($1)}}')
if [[ ! -z "$exceed" ]]
then
    >&2 echo The following disks exceed limit of ${LIMIT}'%'
    >&2 echo "$exceed"
    exit 1
fi
