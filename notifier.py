#!/usr/bin/env python3

from email.message import EmailMessage
from os import environ
from pathlib import Path
from shutil import rmtree
from sys import exit
from tempfile import mkdtemp
import argparse
import configparser
import smtplib
import subprocess


HELP = "Usage: notifier.py <config_file> <script_file>"


class Workdir():
    def __enter__(self):
        self.path = mkdtemp()
        return Path(self.path)

    def __exit__(self, exc_type, exc_val, exc_tb):
        rmtree(self.path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog=Path(__file__).name,
        description="""Generic notification program for server health check reports via email.
stdout of the script goes to message attachment; stderr goes to message body.
Non-zero script exit code triggers notification.""")
    parser.add_argument("config", help="Configuration ini file")
    parser.add_argument("script", help="Target script")
    args = parser.parse_args()

    config_file = Path(args.config)
    script = Path(args.script)

    if not config_file.is_file():
        exit(f"'{args.config}' is not a file")

    if not script.is_file():
        exit(f"'{args.script}' is not a file")

    config = configparser.ConfigParser()
    try:
        config.read(config_file)
    except configparser.MissingSectionHeaderError:
        exit("cannot parse configuration file")

    with Workdir() as workdir:
        try:
            with open(workdir / "stdout", "w") as stdout, \
                 open(workdir / "stderr", "w") as stderr:
                subprocess.run(script.absolute(),
                               stdout=stdout,
                               stderr=stderr,
                               cwd=workdir,
                               timeout=float(environ.get("NOTIFIER_TIMEOUT", config["process"]["timeout"])),
                               check=True)
        except (subprocess.CalledProcessError, subprocess.TimeoutExpired) as err:
            message = EmailMessage()

            message["From"] = config["email"]["address"]
            message["Subject"] = config["email"]["subject"]
            message["To"] = config["email"]["receivers"]

            if isinstance(err, subprocess.TimeoutExpired):
                message.set_content(f"Execution of {script.name} timed out")
            else:
                with open(workdir / "stdout", "r") as stdout, \
                     open(workdir / "stderr", "r") as stderr:
                    message.set_content(f"""{script.name} reported:
{stderr.read()}
""")
                    message.add_attachment(stdout.read(), filename="report.txt")

            try:
                with smtplib.SMTP(config["email"]["server"], int(config["email"]["port"])) as smtp:
                    if config["email"]["starttls"] == "true":
                        smtp.starttls()
                    smtp.login(config["email"]["address"], config["email"]["password"])
                    smtp.send_message(message)
            except Exception as err:
                exit(f"Couldn't send email due to error: {err}")
